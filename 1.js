/*1)setTimeout() виконує код лише один раз  після певної затримки, а setInterval() виконує код циклічно через певні інтервали часу, поки не буде зупинений*/
/*2)Функція не спрацює миттєво. Вона буде виконана якомога швидше, але все ще підпорядковується іншим задачам та подіям у потоці виконання JS.*/
/*3)Якщо не викликати clearInterval(), цикл буде продовжуватися безкінечно або до того моменту, коли програма буде закрита.
Це може призвести до великого навантаження на процесор або споживання пам'яті, що може призвести до збоїв та повільної роботи програми.*/

const images = document.querySelectorAll(".image-to-show");
const time = document.querySelector(".time");
const stop = document.getElementById("stopButton");
const resume = document.getElementById("resumeButton");

let currentImg = 0;
let second = 2;
let milliseconds = 100;
let clock = null;

const hideAllImages = function() {
    images.forEach((imgShow) => {
        imgShow.classList.remove("show");
    });
};

const showNextImage = function() {
    hideAllImages();
    images[currentImg].classList.add("show");
    currentImg = (currentImg + 1) % images.length;
};

function timer() {
    clock = setInterval(() => {
        time.innerHTML = `${second} : ${milliseconds}`;
        milliseconds--;
        if (milliseconds < 0) {
            second--;
            milliseconds = 100;
        }

        if (second < 0) {
            showNextImage();
            second = 2;
        }
    }, 10);
}

const start = function() {
    showNextImage();
    timer();
};

stop.addEventListener("click", () => {
    clearInterval(clock);
});

resume.addEventListener("click", () => {
    clearInterval(clock);
    timer();
});

start();